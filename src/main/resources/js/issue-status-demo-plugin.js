AJS.$(function($){
    var $area = $("#js-status-lozenges");
    var solidLozenge = {
        issueStatus: {
            name: "Solid lozenge",
            description: "Description of solid lozenge",
            statusCategory: {
                key: "key-solid",
                colorName: "green"
            }
        },
        isSubtle: false,
        isCompact: false
    };
    var subtleLozenge = {
        issueStatus: {
            name: "Subtle lozenge",
            description: "Description of subtle lozenge",
            statusCategory: {
                key: "key-subtle",
                colorName: "blue"
            }
        },
        isSubtle: true,
        isCompact: false
    };
    var compactLozenge = {
        issueStatus: {
            name: "Compact lozenge",
            // NOTE: no description here for purpose!
            statusCategory: {
                key: "key-compact",
                colorName: "undefined"
            }
        },
        isSubtle: false,
        isCompact: true
    };

    $area.append("<p>" + JIRA.Template.Util.Issue.Status.issueStatus(solidLozenge) + "</p>");
    $area.append("<p>" + JIRA.Template.Util.Issue.Status.issueStatus(subtleLozenge) + "</p>");
    $area.append("<p>" + JIRA.Template.Util.Issue.Status.issueStatus(compactLozenge) + "</p>");
});
